<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
  
    'facebook' => [
        'client_id' => '178421959668479',        // Your GitHub Client ID
        'client_secret' => '0201f406a20a9abcf8f846c151bc2f03', // Your GitHub Client Secret
        'redirect' => 'http://www.maidandhelper.com/facebook/callback',
    ],
    'google' => [
        'client_id'     => '264216543307-bs7fj5ot6maten3pn8n5pbo18i5uvlkv.apps.googleusercontent.com',
        'client_secret' => 'i-e4Fb9gON5Fn_myFpQmQbj1',
        'redirect'      => 'https://www.maidandhelper.com/google/callback'
    ],
     'twitter' => [
        'client_id'        => 'S3L6C4TE2xasIIKV34rkF2yYS',
        'client_secret'    => 'zlMV2lQsqf5Lsx2G7tVvFranBatXnXCXnQzj0nYyiBtUbVmTTs',
        'access_token'     => '991363404387487747-zfu9bdPReEQuwphvHCvrB2SmzpRgiUL',
        'access_token_key' => 't24SaBtvUjFhHxxZjryTGjiq3AzA1Kx6fGdEb417VveL6',
        'redirect'         => 'http://yallae7gez.com/twitter/callback',
     ],
     'paypal' => [
        'client_id' => 'ASmf-Ew4Esa2xfPYRC0Unh-D5OY2b49k1MUEjW3i_T6nEOIfgloSJ1VABOl1XKfIVrLCHt79yacqONgb',
        'secret' => 'EP2eHrxMIJamiWqKdlKr8RDFzlKWHTdyZ12HOtTogn_vaKkUJ9lbld8u9YEBle1dkLhU-ct_B0bK4ujF',
        'Mode'=>'sandbox'
        ],
        'facebook_poster' => [
    'app_id' => env('1855173067932793'),
    'app_secret' => env('373e70f8d54857e1d31989d487eed023'),
    'access_token' => env('EAAaXRSO2BHkBAEhblyfZCEdUd8BWOmvg3qTza9UbRJZB2PF4rLTWjvcyuaPOiu5Y17xdXcBdVO7APTEIpKp2QbF3NjkIzNhMGw6pJ7FZCvrbj2FW6sDLY31ZAJ0T0kZA7MZBdoP1ghaWWvnIjzqKmK7K3QZAyccKW5BOTZAtwFlTGLZBbIyDbV5Rv0iak4zM7rIB2OLb1aTvvuLacmGSuuXsWtpxw57SiwXYbzD7HRVreJgZDZD'),
],

];
