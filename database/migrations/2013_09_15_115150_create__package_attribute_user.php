<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageAttributeUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Package_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('packages_id')->unsigned();
            $table->foreign('packages_id')
                  ->references('id')->on('packages')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Package_user');
    }
}
