<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PackageCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('PackageCount', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onupdate('cascade');

            $table->integer('attribute_id')->unsigned();
            $table->foreign('attribute_id')
                    ->references('id')->on('attribute')
                    ->onDelete('cascade')
                    ->onupdate('cascade');
                    
            $table->integer('candidate_id')->unsigned();
            $table->foreign('candidate_id')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PackageCount');
    }
}
