<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('working_in')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('employer_nationality_id')->unsigned();
            $table->foreign('employer_nationality_id')
                  ->references('id')->on('nationalities')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
       

            $table->string('company_name')->nullable();
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')
                  ->references('id')->on('countries')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
       
            $table->decimal('salary')->nullable();
            $table->string('role')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
       
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_experiences');
    }
}
