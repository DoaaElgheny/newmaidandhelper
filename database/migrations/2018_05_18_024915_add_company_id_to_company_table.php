<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdToCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('companies', function(Blueprint $table)
        {
           
            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')
                  ->references('id')->on('countries')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
            $table->dropColumn('country_id');

    }
}
