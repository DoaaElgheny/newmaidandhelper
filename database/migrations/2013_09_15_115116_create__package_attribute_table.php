<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Package_attribute', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price');
            $table->integer('packages_id')->unsigned();
            $table->foreign('packages_id')
                  ->references('id')->on('packages')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
            $table->integer('attribute_id')->unsigned();
            $table->foreign('attribute_id')
                  ->references('id')->on('attribute')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Package_attribute');
    }
}
