<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobIndusteriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_industeries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id')->unsigned();
            $table->foreign('job_id')
                  ->references('id')->on('jobs')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
          
            $table->integer('industry_id')->unsigned();
            $table->foreign('industry_id')
                  ->references('id')->on('industries')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_industeries');
    }
}
